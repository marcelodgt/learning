const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.status(404).send({
        error: 'Page not found'
    });
});

app.get('/users', (req, res) => {
    res.send([
        {
            name: 'Marcelo',
            age: 34
        },
        {
            name: 'Monica',
            age: 28
        },
        {
            name: 'Rivelino',
            age: 77
        }
    ]);
});

app.listen(3000);
module.exports.app = app;