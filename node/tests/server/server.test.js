const request = require('supertest');
const expect = require('expect');

var app = require('./server').app;

it('should return Hello World', (done) => {
    request(app)
        .get('/')
        .expect(404)
        .expect((res) => {
            expect(res.body).toInclude({
                error: 'Page not found'
            });
        })
        // .expect({
        //     error: 'Page not found'
        // })
        //.expect('Hello World')
        .end(done);
});

it('should return status 200 and the users array should have my user', (done) => {
    request(app)
        .get('/users')
        .expect(200)
        .expect((res) => {
            expect(res.body).toInclude({
                name: 'Marcelo',
                age: 34
            })
        })
        .end(done);
});
