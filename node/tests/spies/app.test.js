const expect = require('expect');
const rewire = require('rewire');

var app = rewire('./app');
//app._set_
//app._get__

describe('App', () => {

    var db = {
        saveUser: expect.createSpy()
    };
    app.__set__('db', db);

    it('should call the spy correctly', () => {
        var spy = expect.createSpy();
        spy('Marcelo', 34);
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith('Marcelo', 34);
    });

    it('should call saveUser with user object', () => {
        var email = 'marcelo@nodejs.com';
        var pwd = '123abc';

        app.handleSignup(email, pwd);
        expect(db.saveUser).toHaveBeenCalledWith({email, pwd});
    });
});
        
