const utils = require('./utils');
const expect = require('expect');

it('should add two numbers', () => {
    var result = utils.add(33,11);

    // if(result != 44) {
    //     throw new Error(`Expected 44, got ${result}`)
    // }
    expect(result).toBe(44).toBeA('number');
});

it('should square a number' , () => {
    var result = utils.square(3);

    // if(result != 9) {
    //     throw new Error(`Expect 9, got ${result}`);
    // }
    expect(result).toBe(9).toBeA('number');
});

// it('should expect some value' , () => {
//     // expect(12).toNotBe(11);
//     //toBe or notToBe does not work for arrays or objects
//     expect({name:"Marcelo"}).toEqual({name:"Marcelo"})
// });

// it('should have the numbers in the array', () => {
//     //toInclude checks if a number is on the array
//     expect([2,3,4]).toInclude(2);
// });

// it('should not have the number in the array', ()=> {
//     //toExclude checks if a number is not on the array
//     expect([2,3,4,5]).toExclude(1)
// });

// it('should have the same properties and values', () => {
//     //toInclude and Exclude in object
//     //age has to have the same value, for the other properties does not 
//     //matter the values
//     expect({
//         name: 'Marcelo',
//         age: 34,
//         location: 'cps'
//     }).toInclude({
//         age: 33
//     });
// });

it('should verify if first and lastname are set', () =>{
    var user = {
        age:34,
        location:'cps'
    };
    var result = utils.setName(user,'marcelo torres');
    //expect(user).toEqual(result);
    expect(result).toInclude({
        firstName: 'marcelo',
        lastName: 'torres'
    });
});

it('should async add two numbers', (done) => {
    utils.asyncAdd(2,3,(sum) => {
        expect(sum).toBe(5).toBeA('number'); 
        done();
    });
});

it('should async square', (done) => {
    utils.asyncSquare(3, (square) => {
        expect(square).toBe(9).toBeA('number');
        done();
    });
});
