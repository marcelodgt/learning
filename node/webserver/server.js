const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const port = process.env.PORT || 3000;

//start express
var app = express();

//using partials
hbs.registerPartials(__dirname + '/views/partials')

//set configuration for express
app.set('view engine', 'hbs');


//next : when middleware fucntion is done
app.use((req, res, next)=>{
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`;
    fs.appendFile('server.log', log+'\n', (err) => {
        if(err) {
            console.log('Unable to append to server.log');
        }
    });
    
    next();
});

//using middleware to stop the execution. Every request will show the maintenance template.
// app.use((req, res, next) => {
//     res.render('maintenance.hbs');
// });

//set static folder for express
app.use(express.static(__dirname + '/public'));

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});

hbs.registerHelper('screamIt',(text) => {
    return text.toUpperCase();
});

app.get('/', (req, res) => {
    // res.send('<h1>Hello Express</h1>');
    // res.send({
    //     name: 'Marcelo',
    //     likes: [
    //         'Soccer',
    //         'BoardGames'
    //     ]
    // })
    res.render('home.hbs', {
        pageTitle: 'Home',
        username: 'Marcelo',
        welcomeMessage: `Welcome to my website`
    })
});

app.get('/about', (req, res) => {
    //render the page and send these arguments to the page.
    res.render('about.hbs', {
        pageTitle: 'About Page',
    });

});

// /bad - send json with errorMessage
app.get('/bad', (req, res) => {
    res.send({
        status: 'ERROR'
    })
});
//heroku port

app.listen(port, () => {
    console.log(`Server is up on the port ${port}`);
});
