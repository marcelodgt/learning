const request = require('request');

var getWeather = (lat, lng, callback) => {
    request({
        url: `https://api.darksky.net/forecast/7501b9ca007c0f2a522f961099e02ccb/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if(error)
        {
            callback('Unable to connect to forecast.io server');
        } else if (body.code === 400) {
            callback('Unable to fetch weather');
        } else {
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        }
        
    })
}

module.exports = {
    getWeather
}