// var fToCelsius = (temperature) => (((temperature - 32) / 9) * 5).toFixed(2);
// var fToKelvin = (temperature) => (((temperature + 459.67)*5)/9).toFixed(2);

var convertTemperature = (arg, temperature) => {
    if(arg === 'c')
    {
        temperature = (((temperature - 32) / 9) * 5).toFixed(2);
    } else if (arg === 'k') {
        temperature = (((temperature + 459.67)*5)/9).toFixed(2);
    }
    
    return temperature;
}

module.exports = {
    convertTemperature
}