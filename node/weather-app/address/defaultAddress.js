const fs = require('fs');

var getDefaultAddress = () => {
    try {
        var result = fs.readFileSync('./address/address.json');
        
        return JSON.parse(result);
    } catch (e) {
        if(result.length === 0){
            return 'Unable to find default Address'
        }
        else {
            return [];
        }
    }
};


var saveDefaultAddress = (address) => {
    fs.writeFileSync('address.json', JSON.stringify(address));
};

module.exports = {
    getDefaultAddress,
    saveDefaultAddress
};