const request = require('request');

var geocodeAddress = (address, callback) => {
    var encodeAddress = encodeURIComponent(address);
    request({
            url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`,
            json: true 
        }, (error, response, body)=> {
        if(error) {
            callback('Unable to connect to google servers')
        } else if (body.status === 'ZERO_RESULTS') {
            callback('Unable to find Address');
        } else if (body.status === 'OK') {
            callback(undefined, {
                address: body.results[0].formatted_address,
                lat: body.results[0].geometry.location.lat,
                lng: body.results[0].geometry.location.lng
            });
            // console.log(`Address: ${body.results[0].formatted_address}\nLatitude: ${body.results[0].geometry.location.lat}\nLongitute: ${body.results[0].geometry.location.lng}`);
        }

  
    });
};


module.exports = {
    geocodeAddress
}