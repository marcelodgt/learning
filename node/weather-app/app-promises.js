const axios = require('axios');
const yargs = require('yargs');
const convert = require('./weather/convert-temperature');
const da = require('./address/defaultAddress')
const argv = yargs
    .options ({
        a: {
            demand: false,
            alias: 'address',
            describe: 'Addres to fetch weather for',
            string: true
        },
        c: {
            demand: false,
            alias: 'convert',
            describe: 'User wants to Convert the temperature to (c)elsius or (k)elvin',
            choices: ['f', 'c', 'k']
        },
        d: {
            demanad: false,
            alias: 'default',
            describe: 'default address'
        }
    })
    .help()
    .alias('help', 'h')
    .argv;


// console.log(argv.c);
var encodeAddress = '';
if(argv.d) {
        var defaultAddress = da.getDefaultAddress();
        var encodeAddress = encodeURIComponent(JSON.stringify(defaultAddress[0].address));
    
} else {
    var encodeAddress = encodeURIComponent(argv.address);
}

var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`;
axios.get(geocodeUrl).then((response) => {
    if (response.data.status === 'ZERO_RESULTS') {
        throw new Error('Unable to find the address');
    }
    
    var lat = response.data.results[0].geometry.location.lat;
    var lng = response.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.darksky.net/forecast/7501b9ca007c0f2a522f961099e02ccb/${lat},${lng}`;
    console.log(response.data.results[0].formatted_address);
    
    return axios.get(weatherUrl);
}).then((response)=>{
    var temperature = response.data.currently.temperature;
    var apparentTemperature = response.data.currently.apparentTemperature;
    
    temperature = convert.convertTemperature(argv.c, temperature);
    
    apparentTemperature = convert.convertTemperature(argv.c, apparentTemperature);
    
    console.log(`It's currently ${temperature} and it feels like ${apparentTemperature}`);

}).catch((e) => {
    if(e.code==='ENOTFOUND') {
        console.log('Unable to connect to the api server');
    } else {
        console.log(e.message);
    }
});