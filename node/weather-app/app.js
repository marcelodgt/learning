const request = require('request');
const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const weather = require('./weather/weather.js');
const convertTemperature = require('./weather/convert-temperature.js');

const argv = yargs
    .options ({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Addres to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.geocodeAddress(argv.a, (errorMessage, results) => {
    if(errorMessage) {
        console.log(errorMessage);
    } else {
        console.log(results.address);
        weather.getWeather(results.lat, results.lng, (errorMessage, weatherResults) => {
            if(errorMessage) {
                console.log(errorMessage);
            } else {
                
                console.log(`It's currently ${weatherResults.temperature} (${convertTemperature.fToCelsius(weatherResults.temperature)}º C). And it fells like ${weatherResults.apparentTemperature}(${convertTemperature.fToCelsius(weatherResults.apparentTemperature)}º C).`);
            }
        
        });
    }
});