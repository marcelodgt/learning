//order printing: starting app, finishing app, inside of callback
//node is async so it doesn`t need to wait he callback timeout to finish to run the other things that are written //after that
//
console.log('starting app..');
setTimeout(() => {
    console.log('inside of callback');
}, 2000);

setTimeout(() => {
    console.log('testing');
}, 0);

console.log('finishing up');