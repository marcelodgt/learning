const request = require('request');

var geocodeAddress = (address) => {
    return new Promise((resolve, reject) => {
        var encodeAddress = encodeURIComponent(address);
        request({
            url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`,
            json: true 
        }, (error, response, body)=> {
            if(error) {
                reject('Unable to connect to google servers')
            } else if (body.status === 'ZERO_RESULTS') {
                reject('Unable to find Address');
            } else if (body.status === 'OK') {
                console.log('test');
                resolve({
                    address: body.results[0].formatted_address,
                    lat: body.results[0].geometry.location.lat,
                    lng: body.results[0].geometry.location.lng
                });
                // console.log(`Address: ${body.results[0].formatted_address}\nLatitude: ${body.results[0].geometry.location.lat}\nLongitute: ${body.results[0].geometry.location.lng}`);
            }

    
        });
    })
};

geocodeAddress('13024470').then((location) => {
    console.log(JSON.stringify(location, undefined, 2));
}, (errorMessage) => {
    console.log(errorMessage);
});