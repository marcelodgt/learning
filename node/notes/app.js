

//just logging
//console.log('starting app...')

//using require to use the buil-in modules fs and os
const fs = require('fs');
const os = require('os');

//you can use require to get another file instead of built in modules
const notes = require('./notes.js');

//using 3rd party modules
const _ = require('lodash');

//3rd party module yargs
const yargs = require('yargs');


const titleOptions = { 
    describe: 'Title of a note',
    demand: true,
    alias: 't'
};

const bodyOptions = {
    describe: 'Body of a note',
    demand: true,
    alias: 'b'
};

//use json.js
// show all the command line arguments 
// console.log(process.argv);
//get the third element from arguments vector and save to the variable command
//var command = process.argv[2];
// console.log(command);
// const argv = yargs.argv;
const argv = yargs
    .command('add', 'add a new note',{
        title: titleOptions,
        body: bodyOptions
    })
    .command('list', 'list all notes')
    .command('read', 'read an note', {
        title: titleOptions,
    })
    .command('remove', 'Remove a note', {
        title: titleOptions,
    })
    .help()
    .argv;

var command = argv._[0];
// console.log(argv);

if (command === 'add') {
    var note = notes.addNote(argv.title, argv.body);
    if(note){
        console.log(`Note \'${note.title}\' created.`);
        notes.logNote(note);
    }
    else {
        console.log(`Note \'${argv.title}\' already exists.`)
    }
} else if (command === 'list') {
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} note(s).`);
    allNotes.forEach((note) => notes.logNote(note));

} else if (command === 'read' ){ 
    var note = notes.getNote(argv.title);
    if (note) {
        console.log(`Note found...`);
        notes.logNote(note);
    } else{
        console.log('Note not found');
    }
} 
else if (command === 'remove') {
    var noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? console.log(`Note removed.`) : console.log(`Note not removed.`);
}
else {
    console.log('command not recognized');
}