//arrow functions
//model one
// var square = (x) => {
//     var result = x * x;
//     return result;
// }
//model two
//one parementer no need to (). no argument or more than one, you need the ()
var square = x => x * x ;

console.log(square(9));

//more complex example
var user = {
    name: 'Marcelo',
    //method sayHi
    sayHi: () => {
        console.log(arguments);
        console.log(`Hi I'm ${this.name}`);
    },
    sayHiAlt () {
        console.log(arguments);
        console.log(`Hi I'm ${this.name}`);
    }
};

user.sayHi();
user.sayHi(1,2,3)
user.sayHiAlt();
user.sayHiAlt(1,2,3)


