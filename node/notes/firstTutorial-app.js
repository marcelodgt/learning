//just logging
console.log('starting app...')

//using require to use the buil-in modules fs and os
const fs = require('fs');
const os = require('os');

//you can use require to get another file instead of built in modules
const notes = require('./notes.js');

//using 3rd party modules
const _ = require('lodash');

var res = notes.addNote();
console.log('Testing addnote:', res);

var adder = notes.add(1,2);
console.log('testing adder1: ');
console.log(adder);

console.log('Result: ', notes.add(30, -2));
//using the userInfo method to get the user info and save to user variable
// let user = os.userInfo();

//using the appenfile file method from the fs module to write the message on the greetins.txt file
//if the file doesn't exist, a new file will be created.
// fs.appendFile('greeting.txt', `Hello ${user.username}! You're ${notes.age}.`);

//using npm (node package mgmt)
// 1. run npm init on the folder of the app : this will create a file called package.json with the info
// about our app
// in the package.json is where we can define the 3rd party modules that we'll use on the app
// 2. to get the 3rd party modules:
// npm install modulename --save
console.log('testing lodash: ')
console.log(_.isString(true));
console.log(_.isString('Marcelo'));

console.log('Testing lodash.uniq:');
let filteredArray = _.uniq(['Marcelo', 1, 'Torres', 1, 2, 3, 4]);
console.log(`Testing lodash.uniq: ${filteredArray}`);