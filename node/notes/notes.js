// console.log('Starting notes.js');

const fs = require('fs');

// module.exports.age = 34;
// exporting functions
// module.exports.addNote = () => {
//     console.log('addNote');
//     return 'New note';
// };
//
// module.exports.add = (a,b) => {
//     return a + b;
// }

//reusabilty
var fetchNotes = () => {
    try {
        var noteString = fs.readFileSync('notes-data.json');
        return JSON.parse(noteString);
    } catch (e) {
        return []
    }
};

var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
    console.log('Adding Note', title, body);
    var notes = fetchNotes();
    var note= {
        title,
        body,
    };
    

    //this code will read the file and when after that when we try to write will no longer begin a new file
    //but what if the file does not exist? ERROR: ENOENT: no such file or directory
    // var noteString = fs.readFileSync('notes-data.json');
    // notes = JSON.parse(noteString);

    //the try/catch block prevent from the error no such file
    // try {
    //     var noteString = fs.readFileSync('notes-data.json');
    //     notes = JSON.parse(noteString);
    // } catch (e) {

    // }
    
    //notes.filter return true or false if the notle.tittle is equal to one of titles that are in
    //notes array
    var duplicateNotes = notes.filter((note) => note.title === title );

    // console.log(duplicateNotes.length);

    //if duplicatesNotes has one or more elements, it means that the title of the note that
    // we want to add already exists. so we can't add the note on the file.
    if (duplicateNotes.length === 0)
    {
        notes.push(note);
        saveNotes(notes);
        return note;
    }
    
};

var getAll = () => {
    return fetchNotes();
}

var getNote = (title) => {
    console.log(`Read note with the title ${title}`);
    var notes = fetchNotes();
    var wantedNote = notes.filter((note) => note.title === title);

    return wantedNote[0];
}

var removeNote = (title) => {
    var notes = fetchNotes();
    var keepNotes = notes.filter((note) => note.title !== title );
    saveNotes(keepNotes);
    
    isRemoved = notes.length !== keepNotes.length;
    
    return isRemoved;
}
var logNote = (note) => {
    //debugger comand stops the program at this point.
    debugger;
    console.log('----');
    console.log(`Title: ${note.title}`);
    console.log(`Body: ${note.body}`);
}

module.exports = {
    // addNote : addNote (if the object and the value are the same you can do like the line below)
    addNote,
    getAll,
    getNote,
    removeNote,
    logNote
}