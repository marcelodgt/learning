var express = require('express');
var bodyParser = require('body-parser');

var {mongoose} = require('./db/mongoose.js');
var {Todo} = require('./models/todo.js');
var {User} = require('./models/user.js');
var {ObjectID} = require('mongodb');

var app = express();

app.use(bodyParser.json());

//rout to create a new todo
app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });

    todo.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
});

//route to get all todos
app.get('/todos', (req, res) => {
    Todo.find().then((todos) => {
        res.send({todos})
    }, (e) => {
        res.status(400).send(e);
    });
});

//route to get one todo
app.get('/todos/:todoID', (req, res) => {
    
    var todoID = req.params.todoID;
    if(!ObjectID.isValid(todoID)){
        return res.status(404).send();
    }
    Todo.findById(todoID).then((todo) => {
        if(todo) {
            res.status(200).send({todo});
        }
        else {
            
            return res.status(404).send('not found');
        }    
    }).catch((e)=>{
        res.status(400).send();
    })
    
    
});

app.delete('/todos/:id', (req, res) => {
    //get the params list
    //res.send(req.params);
    var todoID = req.params.id;
    if(!ObjectID.isValid(todoID)){
        return res.status(404).send();
    }
    Todo.findByIdAndRemove(todoID).then((todo) => {
        if(todo) {
            res.status(200).send({todo});
        }
        else {
            return res.status(404).send('not found');
        }
    }).catch((e) => {
        res.status(400).send();
    })
})

//start server
app.listen(3000, () => {
    console.log('Started on port 3000');
});

module.exports = {app};