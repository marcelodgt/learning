var mongoose = require('mongoose');

var User = mongoose.model('User', {
    firstName: {
        type: String,
        required: true,
        trim: true,
        minlength: 4
    },
    lastName: {
        type: String,
        required: false,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1
    }
});

module.exports = {User};