const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    var db = client.db('TodoApp')
    console.log('Connected to MongoDB server');

    // argument: {completed: false} => find get only the documents with the property completed equals false
    // db.collection('Todos').find({completed: false}).toArray().then((docs) => {
    //     console.log('TODOS');
    //     console.log(JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch todos', err);
    // });

    //fecthing by the id
    // db.collection('Todos').find({
    //     _id: new ObjectID('5a84fefcf3ae169c59edabec')
    // }).toArray().then((docs) => {
    //     console.log('TODOS');
    //     console.log(JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch todos', err);
    // });

    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`TODOS count: ${count}`);
    // }, (err) => {
    //     console.log('Unable to fetch todos', err);
    // });

    //challenge: get the users with name: "marcelo" from users collection
    db.collection('Users').find({name:'marcelo'}).toArray().then((docs) => {
        console.log('Users');
        console.log(JSON.stringify(docs, undefined, 2));
    });
    

   // client.close();
    

});