const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    var db = client.db('TodoApp')
    console.log('Connected to MongoDB server');

    //deleteMany
    // db.collection('Todos').deleteMany({text:'Eat Supper'}).then((result) => {
    //     console.log(result);
    // });
    
    //deleteOne: works as the deletemany but stops as soon as deletes one record with the criteria
    // db.collection('Todos').deleteOne({text:'Eat supper'}).then((result) => {
    //     console.log(result);
    // });

    //findOneAndDelete
    // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
    //     console.log(result);
    // });

    // chalenge: Use deleteMany to delete users with the same name ('marcelo') and findOneAndDelete
    //to delete one record by id
    // db.collection('Users').deleteMany({name: 'marcelo'}).then((result) => {
    //     console.log(result);
    // });

    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5a7d43795ea53531575844ee')
    }).then((result) => {
        console.log(JSON.stringify(result, undefined, 2));
    });
    // client.close();
    

});