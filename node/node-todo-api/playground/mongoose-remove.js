const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

///delete multiple records
// Todo.remove({}).then((result) => {
//     console.log(result);
// });

//Todo.findOneAndRemove({})
Todo.findByIdAndRemove('5aa9394b188dfcc2d645b854').then((todo) => {
    console.log(todo);
});

//delete one document
Todo.findOneAndRemove({_id:'5aa9394b188dfcc2d645b854'}).then((todo) => {
    console.log(todo);
});