// const MongoClient = require('mongodb').MongoClient;
//the line below is the same as the one above
//destructuring get properties and transform it in variables. you have to use {}
const {MongoClient, ObjectID} = require('mongodb');
//Object destructuring
// var user = {name: 'marcelo', age: 34};
// var {name} = user;
// console.log(name);

//generating an objectID
// var obj = new ObjectID();
// console.log(obj);

//new versin
MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    var db = client.db('TodoApp')
    console.log('Connected to MongoDB server');

    //insert data to TODO collection
    // db.collection('Todos').insertOne({
    //     text: 'Something to do',
    //     completed: false
    // }, (error, result) => {
    //     if (error) {
    //         return console.log('Unable to insert todo', error);
    //     }

    //     console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    //insert new doc into Users(name, age, location)

    // db.collection('Users').insertOne({
    //     name:'MarceloIdTeste',
    //     age: 34,
    //     location: 'Campinas'
    // }, (error, result) => {
    //     if (error) {
    //         return console.log('Unable to insert into Users', error);            
    //     }

    //     // console.log(JSON.stringify(result.ops[0]._id, undefined, 2));
    //     // console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
    //     console.log(JSON.stringify(result.ops[0]._id, undefined, 2));
    // });

    //older version
    //db.close();

    //new version
    client.close();
    

});