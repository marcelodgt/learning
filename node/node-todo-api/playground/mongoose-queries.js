const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

var userId = '4a86c6715d8fa01a6bf9b5201';
var todoId = '6aa4394f4be89043824112bd1';

// if(!ObjectID.isValid(id)){
//     console.log('ID not valid');
// }

//todo.find

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// });

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     if(!todo){
//         return console.log('Todo not found');
//     }
//     console.log('\nTodo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if(!todo){
//         return console.log('ID not found');
//     }
//     console.log('\nTodo by ID', todo);
// }).catch((e) => console.log(e));

//user.findbyId to find
//1.query works but no user
//2. user found
//3. other errors

User.findById(userId).then((user) => {
    if(!user){
        return console.log('User not found');
    }
    console.log('User', user);
}).catch((e)=> console.log(e));