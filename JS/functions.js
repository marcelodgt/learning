/* Imagine you work at a pizza restaurant and you want to write a JavaScript program to take orders so you 
don't have to write orders by hand. You can write a function to perform this task!

Start by writing a function using the keyword const and the name takeOrder. 
Then set the variable = to a set of parentheses followed by an arrow () =>. Inside of its block {}, use console.log() to print 'Order: pizza'. */

/* Fuction without parameter
const takeOrder01 = () => {
    console.log('Order: pizza');
}


takeOrder01();
*/

/* Function with parameter 
const takeOrder02 = (topping) => {
    console.log(`Order: pizza ${topping}`);
}

takeOrder02('topped with mushrooms'); */

/* Function with two parameters 

const takeOrder03 = (topping, crustType) => {
    console.log(`Order: ${crustType} pizza ${topping}`);
}

takeOrder03('topped with mushrooms','Thin Crust');
takeOrder03('topped with bacon','Chicago Crust Style');
takeOrder03('topped with bacon','Italian Crust Style'); */

/* Now that we have the pizza orders, you want to add them up to find the cost of the pizzas for the check. 
Let's imagine that each pizza is $7.50, no matter the topping and crust type.

We will need to do three things to write this in JavaScript:

Create a variable to hold the number of pizzas ordered.
Whenever a pizza is ordered, add one to the number of pizzas ordered.
Take the total number of pizzas and multiply them by 7.5, since each pizza is $7.50.
Begin by creating a variable named orderCount set equal to 0 at the top of your code. */

/* let orderCount = 0;

const takeOrder = (crustType,topping) => {
    console.log(`Order: ${crustType} pizza ${topping}`);
    orderCount++;
}

const getSubTotal = (itemCount) => {
    return itemCount * 7.5;
}

takeOrder('topped with mushrooms','Thin Crust');
takeOrder('topped with bacon','Chicago Crust Style');
takeOrder('topped with bacon','Italian Crust Style');

console.log(getSubTotal(orderCount)); */



let orderCount = 0;

const takeOrder = (crustType,topping) => {
    console.log(`Order: ${crustType} pizza ${topping}`);
    orderCount++;
}

const getSubTotal = (itemCount) => {
    return itemCount * 7.5;
}
const getTax = (orderCount) => {
  return getSubTotal(orderCount) * 0.06;
}

const getTotal = () => {
  return getSubTotal(orderCount) + getTax(orderCount);
}
takeOrder('topped with mushrooms','Thin Crust');
takeOrder('topped with bacon','Chicago Crust Style');
takeOrder('topped with bacon','Italian Crust Style');

console.log(getTotal());