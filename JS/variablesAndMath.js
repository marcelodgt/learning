/*
Variables:
 - const = constant
 - let = can receive new values after definition

 Math:
 +=, -= *=, /=
 ++ --
*/

//
const name = 'Marcelo';
let money = 100;
let interest = 1.05;

money *= interest;
console.log('Marcelo has '+money+' reais');
//new way to string interpolation

console.log(`Marcelo has ${money} reais`);

