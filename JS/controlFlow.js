// If and Else
let isSoccerFan = false;

if (isSoccerFan) {
    console.log('Goal!');
}
else{
    console.log('No goal!')
}

//falsy values
//false, 0, -0, undefined, emory strings, null, NaN (not a number), document.all

let wordCount = 0;

if (wordCount) {
  console.log("Great! You've started your work!");
} else {
  console.log('Better get to work!');
}


let favoritePhrase = 'Marcelo';

if (!favoritePhrase) {
  console.log("This string doesn't seem to be empty.");
} else {
  console.log('This string is definitely empty.');
}

let hungerLevel = 5;

if (hungerLevel > 7)
    console.log('Time to eat');
else   
    console.log('We can eat later');


/*
If the moon is mostly full, log Arms and legs are getting hairier. If the moon is mostly new, 
log Back on two feet.
If someone enters in an invalid moon phase, make sure to log Invalid moon phase in the else code block.    
*/

let moonPhase = 'full';
let isFoggyNight = false;

if (moonPhase==='full' || isFoggyNight) {
    console.log('Howl!')
}
else if(moonPhase==='mostly full'){
    console.log('Arms and legs are getting hairier.');
}
else if(moonPhase==='mostly new'){
    console.log('Back on two feet.')
}
else{
    console.log('Invalid moon phase.')
}

//SWITCH

let moonPhase2 = 'mostly new';

switch (moonPhase2) {
    case 'full':
        console.log('Howl!')
        break;
    case 'mostly full':
        console.log('Arms and legs are getting hairier.');
        break;
    case 'mostly new':
        console.log('Back on two feet.');
        break;
    default:
        console.log('Invalid moon phase.')
        break;
}

//Ternary Operator
// variable_is_truthy ? block_for yes : block_for_no;

let isLocked = false;

isLocked ? console.log('You will need a key to open the door.') : console.log('You will not need a key to open the door.');

let isCorrect = true;

isCorrect ? console.log('Correct!') : console.log('Incorrect!');


let favoritePhrase = 'Love That!';

if (favoritePhrase === 'Love That!') {
  console.log('I love that!');
} else {
  console.log("I don't love that!");
}

favoritePhrase === 'Love That!' ? console.log('I love that!') : console.log("I don't love that!");




